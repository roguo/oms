# salt api config
SALT_API = {'apiurl': 'https://10.10.0.31:8000',
            'user': 'saltapi',
            'password': 'saltapi'
            }

# cobbler api config
Cobbler_API = {'apiurl': '',
               'user': '',
               'password': ''
               }

# salt result
RETURNS_MYSQL = {
    'host': 'localhost',
    'port': '3306',
    'database': 'salt',
    'user': 'salt',
    'password': 'salt'

}

OMS_MYSQL = {"host": "localhost",
             "port": 3306,
             "database": "oms",
             "user": "root",
             "password": "root"
             }
