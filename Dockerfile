FROM alpine
MAINTAINER wilean <roguo.wei@gmail.com>
RUN apk add --update openssh python3 sshpass py-mysqldb py-psutil py-crypto && \
    rm -rf /var/cache/apk/*
COPY . /oms
WORKDIR /oms
#apk add curl
#curl -O https://bootstrap.pypa.io/get-pip.py
#python3 get-pip.py
#rm get-pip.py
RUN python3 /oms/install/docker/get-pip.py && \
    pip install -r /oms/install/docker/piprequires.txt && \
    rm -rf /oms/docs && \
    rm -rf /oms/.git && \
    rm -rf /oms/.gitosc && \
    chmod -R 777 /oms
VOLUME /data
EXPOSE 80 22
ENTRYPOINT ["python3", "manage.py", "runserver", "0.0.0.0:80"]
