#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from publish.views import *

urlpatterns = [
    url(r'^project/$', project_list, name='project_list'),
    url(r'^project/get_list/$', project_getlist, name='project_getlist'),
    url(r'^project/conf/$', project_conf, name='project_conf'),
    url(r'^project/edit/$', project_edit, name='project_edit'),
    url(r'^project/preview/$', project_preview, name='project_preview'),
    url(r'^project/del/$', project_del, name='project_del'),
    url(r'^task/$', task_list, name='task_list'),
    url(r'^task/get_list/$', task_getlist, name='task_getlist'),
    url(r'^task/submit/$', task_submit, name='task_submit'),
    url(r'^task/deploy/$', task_deploy, name='task_deploy'),
    url(r'^check/$', check, name='file_check'),
]
