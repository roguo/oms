#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@version: ??
@author: rongguo
@license: Apache Licence 
@contact: rongo.wei@gmail.com
@software: PyCharm
@project: oms
@file: forms.py
@time: 17-4-22 下午9:35
"""

from django import forms
from publish.models import *


class ProjectModelForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'repo_url', 'repo_username', 'repo_password', 'deploy_from', 'excludes',
                  'release_user', 'release_to', 'release_library', 'keep_version_num', 'hosts', 'pre_deploy',
                  'post_deploy', 'pre_release', 'post_release', 'post_release_delay']
        # fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'repo_url': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'git@git.oschina.net:roguo/oms.git'}),
            'repo_username': forms.TextInput(attrs={'class': 'form-control'}),
            'repo_password': forms.TextInput(attrs={'class': 'form-control'}),
            'deploy_from': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '/data/www/deploy'}),
            'excludes': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': '.git\n.svn\nREADME.md'}),
            'release_user': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'www'}),
            'release_to': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '/data/www/wale'}),
            'release_library': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '/data/releases'}),
            'keep_version_num': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '20'}),
            'hosts': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': '192.168.101.2\n192.168.2.3:8080'}),
            'pre_deploy': forms.TextInput(attrs={'class': 'form-control'}),
            'post_deploy': forms.TextInput(attrs={'class': 'form-control'}),
            'pre_release': forms.TextInput(attrs={'class': 'form-control'}),
            'post_release': forms.TextInput(attrs={'class': 'form-control'}),
            'post_release_delay': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '0'}),
        }
