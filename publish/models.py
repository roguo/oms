from django.db import models
# from user.models import User

# Create your models here.
PROJECT_ENV = (
    (1, u'开发环境'),
    (2, u'测试环境'),
    (3, u'预发布环境'),
    (4, u'生产环境')
)


class Project(models.Model):
    user_id = models.IntegerField(verbose_name=u'添加项目的用户id')
    # user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(default='master', null=True, max_length=50, verbose_name=u'项目名字')
    env = models.IntegerField(default=1, verbose_name=u'项目环境1：开发，2：测试，3：预发布，4：生产')
    status = models.IntegerField(default=1, verbose_name=u'状态0：无效，1有效')
    version = models.CharField(null=True, max_length=30, verbose_name=u'线上当前版本，用于快速回滚')
    repo_url = models.CharField(default='', null=True, max_length=255, verbose_name=u'git地址')
    repo_username = models.CharField(default='', blank=True, null=True, max_length=50,
                                     verbose_name=u'版本管理系统的用户名，一般为svn的用户名')
    repo_password = models.CharField(default='', blank=True, null=True, max_length=100,
                                     verbose_name=u'版本管理系统的密码，一般为svn的密码')
    repo_mode = models.CharField(default='branch', null=True, max_length=50, verbose_name=u'上线方式：branch/tag')
    repo_type = models.CharField(default='git', null=True, max_length=10, verbose_name=u'上线方式：git/svn')
    deploy_from = models.CharField(max_length=255, verbose_name=u'宿主机存放clone出来的文件')
    excludes = models.TextField(blank=True, null=True, verbose_name=u'要排除的文件')
    release_user = models.CharField(max_length=50, verbose_name=u'目标机器用户')
    release_to = models.CharField(max_length=255, verbose_name=u'目标机器的目录，相当于nginx的root，可直接web访问')
    release_library = models.CharField(max_length=255, verbose_name=u'目标机器版本发布库')
    hosts = models.TextField(null=True, verbose_name=u'目标机器列表')
    pre_deploy = models.TextField(blank=True, null=True, verbose_name=u'部署前置任务')
    post_deploy = models.TextField(blank=True, null=True, verbose_name=u'同步之前任务')
    pre_release = models.TextField(blank=True, null=True, verbose_name=u'同步之前目标机器执行的任务')
    post_release = models.TextField(blank=True, null=True, verbose_name=u'同步之后目标机器执行的任务')
    post_release_delay = models.IntegerField(blank=True, default=0, verbose_name=u'每台目标机执行post_release任务间隔/延迟时间 单位:秒')
    audit = models.IntegerField(null=True, verbose_name=u'是否需要审核任务0不需要，1需要')
    saltstack = models.IntegerField(verbose_name=u'是否启用Saltstack 0关闭，1开启')
    keep_version_num = models.IntegerField(verbose_name=u'线上版本保留数')
    created_at = models.DateTimeField(auto_now=True, null=True, verbose_name=u'创建时间')
    updated_at = models.DateTimeField(auto_now=True, null=True, verbose_name=u'修改时间')

    class Meta:
        verbose_name_plural = u'项目表'
        # db_table = 't_project'


class Record(models.Model):
    user_id = models.IntegerField(verbose_name=u'用户id')
    # user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    task_id = models.IntegerField(verbose_name=u'任务id')
    status = models.IntegerField(default=1, verbose_name=u'状态1：成功，0失败')
    action = models.IntegerField(null=True, verbose_name=u'任务执行到的阶段')
    command = models.TextField(null=True, verbose_name=u'运行命令')
    duration = models.IntegerField(null=True, default=0, verbose_name=u'耗时，单位ms')
    memo = models.TextField(null=True, verbose_name=u'日志/备注')
    created_at = models.DateTimeField(auto_now=True, null=True, verbose_name=u'创建时间')

    class Meta:
        verbose_name_plural = u'记录表'
        # db_table = 't_record'


class Task(models.Model):
    user_id = models.IntegerField(verbose_name=u'用户id')
    # user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    project_id = models.IntegerField(default=0, verbose_name=u'项目id')
    action = models.IntegerField(default=0, verbose_name=u'0全新上线，2回滚')
    status = models.IntegerField(default=0, verbose_name=u'状态0：新建提交，1审核通过，2审核拒绝，3上线完成，4上线失败')
    title = models.CharField(max_length=100, null=True, default='', verbose_name=u'上线的标题')
    link_id = models.CharField(max_length=20, null=True, default='', verbose_name=u'上线的软链号')
    ex_link_id = models.CharField(max_length=20, null=True, default='', verbose_name=u'上一次上线的软链号')
    commit_id = models.CharField(max_length=20, null=True, default='', verbose_name=u'git commit id')
    branch = models.CharField(max_length=50, null=True, default='master', verbose_name=u'选择上线的分支')
    file_transmission_mode = models.IntegerField(default=1, verbose_name=u'上线文件模式: 1.全量所有文件 2.指定文件列表')
    file_list = models.TextField(null=True, verbose_name=u'文件列表，svn上线方式可能会产生')
    enable_rollback = models.IntegerField(default=1, verbose_name=u'能否回滚此版本:0no 1yes')
    created_at = models.DateTimeField(auto_now=True, null=True, verbose_name=u'创建时间')
    updated_at = models.DateTimeField(auto_now=True, null=True, verbose_name=u'修改时间')

    class Meta:
        verbose_name_plural = u'任务表'
        # db_table = 't_task'
